#Repository moved to https://github.com/CollinQuiason/Predictive-Parking March 7, 2018


# PredictiveParking

#---------------------------------------------------------------------------------------------------

##  **GUI:**

Buttons are self-explanatory

Push 'Enter' after altering text box for time selection to update visualization

Slider located on top left for changing transparency (Updated visualization on mouse-click-up)

###---------------------------------------------------------------------------------------------------

##  **GETTING IT RUNNING:**

-You must install node.js 'http-server' module and add it to your path.

-npm install http-server

-Run localserver.bat and open 'localhost:8080' in browser

-Otherwise, find another way to host a local server

###---------------------------------------------------------------------------------------------------

##  **NOTES/WARNINGS:**

WILL NOT LOAD WITHOUT BEING RAN ON A SERVER

